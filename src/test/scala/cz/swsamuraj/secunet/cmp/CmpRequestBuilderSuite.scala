package cz.swsamuraj.secunet.cmp

import java.math.BigInteger

import org.bouncycastle.asn1.ASN1Integer
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.{CRLReason, Extension}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CmpRequestBuilderSuite extends FunSuite {

  trait Fixture {
    val cmpBuilder = new CmpRequestBuilder
    val issuer = new X500Name("C=CZ, CN=SoftWare Samuraj Enc Sub-CA")
    val asn1Csn = new ASN1Integer(new BigInteger("1"))

    cmpBuilder.CERT_DIR = "./src/test/resources"
  }

  test("pvno equals 2") {
    new Fixture {
      val header = cmpBuilder.pkiHeader("42")

      assert(header.getPvno.getValue.intValue === CmpRequestBuilder.PVNO)
    }
  }

  test("null transactionID throws IAE") {
    new Fixture {
      assertThrows[IllegalArgumentException] {
        cmpBuilder.stringToByteArray(null)
      }
    }
  }

  test("empty transactionID returns an empty array") {
    new Fixture {
      assert(cmpBuilder.stringToByteArray("") === Array[Byte]())
    }
  }

  test("transactionID from string to byte array") {
    new Fixture {
      assert(cmpBuilder.stringToByteArray("1") === Array[Byte](49))
      assert(cmpBuilder.stringToByteArray("12") === Array[Byte](49, 50))
      assert(cmpBuilder.stringToByteArray("123") === Array[Byte](49, 50, 51))
    }
  }

  test("find certificate path") {
    new Fixture {
      assert(cmpBuilder.certPath("Enc") === "./src/test/resources/John-Coltrane-Enc.pem")
    }
  }

  test("read a certificate holder") {
    new Fixture {
      val certHolder = cmpBuilder.readCert("Enc")

      assert(certHolder.getSerialNumber === new BigInteger("1"))
      assert(certHolder.getIssuer === issuer)
    }
  }

  test("assemble a certificate template") {
    new Fixture {
      val certTemplate = cmpBuilder.certTemplate("Enc")

      assert(certTemplate.getSerialNumber === asn1Csn)
      assert(certTemplate.getIssuer === issuer)
    }
  }

  test("build rev detail") {
    new Fixture {
      val revDetail = cmpBuilder.buildRevDetail("Enc")

      assert(revDetail.getCertDetails.getIssuer === issuer)
      assert(revDetail.getCertDetails.getSerialNumber === asn1Csn)

      val extension = revDetail.getCrlEntryDetails.getExtension(Extension.reasonCode)
      val reason = CRLReason.getInstance(extension.getParsedValue)

      assert(reason.getValue.intValue === CRLReason.removeFromCRL)
    }
  }

}