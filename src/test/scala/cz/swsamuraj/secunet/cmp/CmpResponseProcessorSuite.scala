package cz.swsamuraj.secunet.cmp

import java.io.FileReader

import org.bouncycastle.asn1.x509.Certificate
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.openssl.PEMParser
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CmpResponseProcessorSuite extends FunSuite {

  trait Fixture {
    val IssuerSig = "C=NO, CN=ID Card Sig CA"
    val IssuerEnc = "C=NO, CN=ID Card Enc CA"
    val IssuerAuth = "C=NO, CN=ID Card Auth CA"

    val processor = new CmpResponseProcessor
  }

  trait CertificateFixture extends Fixture {
    val cert = readCert
  }

  test("nominal cert type") {
    new Fixture {
      assert(processor.certType(IssuerSig) === "Sig")
      assert(processor.certType(IssuerEnc) === "Enc")
      assert(processor.certType(IssuerAuth) === "Auth")
    }
  }

  test("unknown cert type") {
    new Fixture {
      assert(processor.certType("Some CA") === "UNKNOWN")
    }
  }

  test("certificate file name") {
    new CertificateFixture {
      assert(processor.certificateFile(cert) === "./cert/C=US,CN=John Coltrane-Enc.pem")
    }
  }

  private def readCert = {
    val parser = new PEMParser(new FileReader("src/test/resources/John-Coltrane-Enc.pem"))
    val holder = parser.readObject().asInstanceOf[X509CertificateHolder]
    Certificate.getInstance(holder.toASN1Structure)
  }

}