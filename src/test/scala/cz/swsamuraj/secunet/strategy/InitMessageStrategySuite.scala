package cz.swsamuraj.secunet.strategy

import java.nio.file.Paths

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class InitMessageStrategySuite extends FunSuite {

  test("write and read the init transaction id") {
    val strategy = new InitMessageStrategy("John Coltrane")
    val transactionIDPath = Paths.get("./src/test/resources/transactionID.txt")
    val pendingIDPath = Paths.get("./src/test/resources/pendingID.txt")

    strategy.TRANSACTION_ID_PATH = transactionIDPath
    strategy.PENDING_ID_PATH = pendingIDPath
    strategy.writeID("42", transactionIDPath)

    val transactionID = strategy.transactionID()

    assert(transactionID === "42")
    assert(strategy.readID(transactionIDPath) === "43")
    assert(strategy.readID(pendingIDPath) === "42")
  }

}
