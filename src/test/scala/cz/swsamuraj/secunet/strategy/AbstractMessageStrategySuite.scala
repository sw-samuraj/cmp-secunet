package cz.swsamuraj.secunet.strategy

import java.nio.file.Paths

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class AbstractMessageStrategySuite extends FunSuite {

  test("write and read transaction ID to/from file") {
    val strategy = new InitMessageStrategy("John Coltrane")
    val transactionID = "42"
    val path = Paths.get("./src/test/resources/transactionID.txt")

    strategy.writeID(transactionID, path)

    assert(strategy.readID(path) === transactionID)
  }

}
