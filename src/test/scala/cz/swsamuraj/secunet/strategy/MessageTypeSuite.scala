package cz.swsamuraj.secunet.strategy

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MessageTypeSuite extends FunSuite {

  test("get message type by string") {
    assert(MessageType.typeOf("init") === MessageType.INIT)
    assert(MessageType.typeOf("poll") === MessageType.POLL)
    assert(MessageType.typeOf("revoke") === MessageType.REVOKE)
  }

  test("unknown message type returns null") {
    assert(MessageType.typeOf("unknown") === null)
  }

}
