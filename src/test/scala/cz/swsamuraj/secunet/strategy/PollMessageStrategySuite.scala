package cz.swsamuraj.secunet.strategy

import java.nio.file.Paths

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PollMessageStrategySuite extends FunSuite {

  test("write and read the poll transaction id") {
    val strategy = new PollMessageStrategy
    val pendingIDPath = Paths.get("./src/test/resources/pendingID.txt")

    strategy.PENDING_ID_PATH = pendingIDPath
    strategy.writeID("42", pendingIDPath)

    val transactionID = strategy.transactionID()

    assert(transactionID === "42")
    assert(strategy.readID(pendingIDPath) === "")
  }

}
