package cz.swsamuraj.secunet.strategy

import java.nio.file.Paths

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class RevokeMessageStrategySuite extends FunSuite {

  trait Fixture {
    val strategy = new RevokeMessageStrategy
    val transactionIDPath = Paths.get("./src/test/resources/transactionID.txt")
    val csnPath = Paths.get("./src/test/resources/csn.txt")

    strategy.TRANSACTION_ID_PATH = transactionIDPath
    strategy.CSN_PATH = csnPath
  }

  test("write and read the revoke transaction id") {
    new Fixture {
      strategy.writeID("42", transactionIDPath)

      val transactionID = strategy.transactionID

      assert(transactionID === "42")
      assert(strategy.readID(transactionIDPath) === "43")
    }
  }

}
