package cz.swsamuraj.secunet.http;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SecunetClientBuilder {

    private static final Properties prop = new Properties();

    public SecunetClientBuilder() throws Exception {
        prop.load(new FileInputStream("./security/connection.properties"));
    }

    public CloseableHttpClient httpClient() throws Exception {
        return HttpClients.custom()
                .setSSLSocketFactory(socketFactory())
                .build();
    }

    public HttpPost httpPost() {
        String cmpUri = prop.getProperty("cmp.uri");
        HttpPost httpPost = new HttpPost(cmpUri);
        httpPost.setConfig(config());
        httpPost.addHeader("Content-Type", "application/pkixcmp");

        return httpPost;
    }

    public HttpHost targetHost() {
        String proxyUrl = prop.getProperty("secunet.url");

        return HttpHost.create(proxyUrl);
    }

    public ResponseHandler<byte[]> responseHandler() {
        return new ResponseHandler<byte[]>() {
            @Override
            public byte[] handleResponse(HttpResponse response) throws IOException {
                HttpEntity entity = response.getEntity();

                return EntityUtils.toByteArray(entity);
            }
        };
    }

    private SSLConnectionSocketFactory socketFactory() throws Exception {
        return new SSLConnectionSocketFactory(sslContext());
    }

    private SSLContext sslContext() throws Exception {
        String trustStore = prop.getProperty("trustStore.file");
        char[] password = prop.getProperty("trustStore.password").toCharArray();

        return SSLContexts.custom()
                .loadTrustMaterial(
                        new File(trustStore),
                        password,
                        new TrustSelfSignedStrategy())
                .build();
    }

    private HttpHost proxyHost() {
        String proxyUrl = prop.getProperty("proxy.url");

        return HttpHost.create(proxyUrl);
    }

    private RequestConfig config() {
        return RequestConfig.custom()
                .setProxy(proxyHost())
                .build();
    }

    public HttpEntity httpEntity(byte[] derMessages) {
        return new ByteArrayEntity(derMessages);
    }
}
