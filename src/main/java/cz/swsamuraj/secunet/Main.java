package cz.swsamuraj.secunet;

import cz.swsamuraj.secunet.strategy.*;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            printWrongArgument("Missing");
        } else {
            MessageType messageType = MessageType.typeOf(args[0]);

            if (messageType == null) {
                printWrongArgument("Wrong");
            } else if (messageType.equals(MessageType.INIT) && args.length != 2) {
                printMissingSubject();
            } else {
                MessageStrategy strategy;

                switch (messageType) {
                    case INIT:
                        strategy = new InitMessageStrategy(args[1]);
                        break;
                    case POLL:
                        strategy = new PollMessageStrategy();
                        break;
                    case REVOKE:
                        strategy = new RevokeMessageStrategy();
                        break;
                    default:
                        strategy = new UnimplementedMessageStrategy();
                }

                strategy.assembleAndProcessMessage();
            }
        }
    }

    private static void printWrongArgument(String adjective) {
        System.out.format("%n%s message type argument! Available values: init, poll, revoke%n", adjective);
    }

    private static void printMissingSubject() {
        System.out.println("\nCertificate subject parameter is missing!");
    }

}
