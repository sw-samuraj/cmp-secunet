package cz.swsamuraj.secunet.strategy;

public class InitMessageStrategy extends AbstractMessageStrategy {

    private final String subject;

    public InitMessageStrategy(String subject) {
        this.subject = subject;
    }

    @Override
    protected byte[] requestBody() throws Exception {
        return cmpBuilder.derMessages(MessageType.INIT, transactionID(), subject);
    }

    @Override
    protected void processResponse(byte[] responseBody) throws Exception {
        processor.pkiMessages(MessageType.INIT, responseBody);
    }

    @Override
    protected String transactionID() throws Exception {
        String transactionID = readID(TRANSACTION_ID_PATH);
        String incrementedID = Integer.toString(Integer.parseInt(transactionID) + 1);

        writeID(incrementedID, TRANSACTION_ID_PATH);
        writeID(transactionID, PENDING_ID_PATH);

        return transactionID;
    }

}
