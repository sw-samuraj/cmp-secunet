package cz.swsamuraj.secunet.strategy;

public class UnimplementedMessageStrategy implements MessageStrategy {

    @Override
    public void assembleAndProcessMessage() throws Exception {
        System.out.println("This message strategy has not yet been implemented!");
    }

}
