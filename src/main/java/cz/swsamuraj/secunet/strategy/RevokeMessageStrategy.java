package cz.swsamuraj.secunet.strategy;

import java.nio.file.Path;
import java.nio.file.Paths;

public class RevokeMessageStrategy extends AbstractMessageStrategy {

    Path CSN_PATH = Paths.get("./security/csn.txt");

    @Override
    protected byte[] requestBody() throws Exception {
        return cmpBuilder.derMessages(MessageType.REVOKE, transactionID());
    }

    @Override
    protected void processResponse(byte[] responseBody) throws Exception {
        processor.pkiMessages(MessageType.REVOKE, responseBody);
    }

    @Override
    protected String transactionID() throws Exception {
        String transactionID = readID(TRANSACTION_ID_PATH);
        String incrementedID = Integer.toString(Integer.parseInt(transactionID) + 1);

        writeID(incrementedID, TRANSACTION_ID_PATH);

        return transactionID;
    }

}
