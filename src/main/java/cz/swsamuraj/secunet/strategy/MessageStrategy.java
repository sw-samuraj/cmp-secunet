package cz.swsamuraj.secunet.strategy;

public interface MessageStrategy {

    void assembleAndProcessMessage() throws Exception;
}
