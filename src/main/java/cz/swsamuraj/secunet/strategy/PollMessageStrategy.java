package cz.swsamuraj.secunet.strategy;

public class PollMessageStrategy extends AbstractMessageStrategy {

    @Override
    protected byte[] requestBody() throws Exception {
        return cmpBuilder.derMessages(MessageType.POLL, transactionID());
    }

    @Override
    protected void processResponse(byte[] responseBody) throws Exception {
        processor.pkiMessages(MessageType.POLL, responseBody);
    }

    @Override
    protected String transactionID() throws Exception {
        String transactionID = readID(PENDING_ID_PATH);

        writeID("", PENDING_ID_PATH);

        return transactionID;
    }

}
