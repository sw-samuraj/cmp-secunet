package cz.swsamuraj.secunet.strategy;

import cz.swsamuraj.secunet.cmp.CmpRequestBuilder;
import cz.swsamuraj.secunet.cmp.CmpResponseProcessor;
import cz.swsamuraj.secunet.http.SecunetClientBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public abstract class AbstractMessageStrategy implements MessageStrategy {

    protected final CmpRequestBuilder cmpBuilder = new CmpRequestBuilder();
    protected final CmpResponseProcessor processor = new CmpResponseProcessor();

    protected Path TRANSACTION_ID_PATH = Paths.get("./security/transactionID.txt");
    protected Path PENDING_ID_PATH = Paths.get("./security/pendingID.txt");

    @Override
    public void assembleAndProcessMessage() throws Exception {
        SecunetClientBuilder clientBuilder = new SecunetClientBuilder();

        HttpPost httpPost = clientBuilder.httpPost();
        HttpEntity requestBody = clientBuilder.httpEntity(requestBody());
        httpPost.setEntity(requestBody);
        HttpHost target = clientBuilder.targetHost();

        CloseableHttpClient client = clientBuilder.httpClient();
        ResponseHandler<byte[]> responseHandler = clientBuilder.responseHandler();

        try {
            byte[] responseBody = client.execute(target, httpPost, responseHandler);
            processResponse(responseBody);
        } finally {
            client.close();
        }
    }

    protected abstract byte[] requestBody() throws Exception;

    protected abstract void processResponse(byte[] responseBody) throws Exception;

    protected abstract String transactionID() throws Exception;

    protected String readID(Path path) throws Exception {
        List<String> lines = Files.readAllLines(path);

        if (lines.isEmpty()) {
            return "";
        } else {
            return lines.get(0);
        }
    }

    protected void writeID(String stringID, Path path) throws Exception {
        Files.write(path, cmpBuilder.stringToByteArray(stringID));
    }

}
