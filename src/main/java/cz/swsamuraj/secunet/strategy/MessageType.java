package cz.swsamuraj.secunet.strategy;

import java.util.HashMap;
import java.util.Map;

public enum MessageType {

    INIT("init"),
    POLL("poll"),
    REVOKE("revoke");

    private String type;

    MessageType(String type) {
        this.type = type;
    }

    private static final Map<String, MessageType> lookup = new HashMap<>();

    static {
        for (MessageType messageType : values()) {
            lookup.put(messageType.type, messageType);
        }
    }

    public static MessageType typeOf(String type) {
        return lookup.get(type);
    }
}
