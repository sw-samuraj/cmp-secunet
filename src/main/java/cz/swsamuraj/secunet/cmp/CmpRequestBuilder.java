package cz.swsamuraj.secunet.cmp;

import cz.swsamuraj.secunet.strategy.MessageType;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.cmp.*;
import org.bouncycastle.asn1.crmf.CertReqMessages;
import org.bouncycastle.asn1.crmf.CertReqMsg;
import org.bouncycastle.asn1.crmf.CertTemplate;
import org.bouncycastle.asn1.crmf.CertTemplateBuilder;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.crmf.AuthenticatorControl;
import org.bouncycastle.cert.crmf.CertificateRequestMessage;
import org.bouncycastle.cert.crmf.CertificateRequestMessageBuilder;
import org.bouncycastle.cert.crmf.Control;
import org.bouncycastle.openssl.PEMParser;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.Certificate;
import java.util.*;
import java.security.cert.Certificate;

public class CmpRequestBuilder {

    static final int PVNO = 2;
    static final long CERT_REQ_ID = 42L;

    private final Properties prop = new Properties();

    String CERT_DIR = "./cert";

    public CmpRequestBuilder() {
        try {
            prop.load(new FileInputStream("./security/ca.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] derMessages(MessageType messageType, String transactionID) throws Exception {
        return derMessages(messageType, transactionID, null);
    }

    public byte[] derMessages(MessageType messageType, String transactionID, String subject) throws Exception {
        PKIHeader header = pkiHeader(transactionID);
        PKIBody body = pkiBody(messageType, subject);
        DERBitString protection = protection(header, body);
        CMPCertificate certificate = signingCertificate();

        PKIMessage message = pkiMessage(header, body, protection, certificate);
        PKIMessages messages = pkiMessages(message);

        return messages.getEncoded(ASN1Encoding.DER);
    }

    private List<CertReqMsg> certMessages(String stringSubject) throws Exception {
        X500Name issuerSig = x500Name(prop.getProperty("ISSUER_SIG"));
        X500Name issuerEnc = x500Name(prop.getProperty("ISSUER_ENC"));
        X500Name issuerAuth = x500Name(prop.getProperty("ISSUER_AUTH"));
        X500Name subject = x500Name(stringSubject);

        CertReqMsg certMsgSig = certificateRequestMessage(issuerSig, subject).toASN1Structure();
        CertReqMsg certMsgEnc = certificateRequestMessage(issuerEnc, subject).toASN1Structure();
        CertReqMsg certMsgAuth = certificateRequestMessage(issuerAuth, subject).toASN1Structure();

        List<CertReqMsg> certMessages = new ArrayList<>();
        certMessages.add(certMsgSig);
        certMessages.add(certMsgEnc);
        certMessages.add(certMsgAuth);

        return certMessages;
    }

    private List<RevDetails> revDetails() throws Exception {
        RevDetails revDetailSig = buildRevDetail("Sig");
        RevDetails revDetailEnc = buildRevDetail("Enc");
        RevDetails revDetailAuth = buildRevDetail("Auth");

        List<RevDetails> revDetails = new ArrayList<>();
        revDetails.add(revDetailSig);
        revDetails.add(revDetailEnc);
        revDetails.add(revDetailAuth);

        return revDetails;
    }

    RevDetails buildRevDetail(String certType) throws Exception {
        CertTemplate certTemplate = certTemplate(certType);

        CRLReason crlReason = CRLReason.lookup(CRLReason.removeFromCRL);
        ExtensionsGenerator generator = new ExtensionsGenerator();
        generator.addExtension(Extension.reasonCode, false, crlReason);
        Extensions extensions = generator.generate();

        return new RevDetails(certTemplate, extensions);
    }

    CertTemplate certTemplate(String certType) throws Exception {
        X509CertificateHolder certHolder = readCert(certType);
        X500Name issuer = certHolder.getIssuer();
        BigInteger csn = certHolder.getSerialNumber();

        CertTemplateBuilder certBuilder = new CertTemplateBuilder();
        certBuilder.setIssuer(issuer);
        certBuilder.setSerialNumber(new ASN1Integer(csn));

        return certBuilder.build();
    }

    X509CertificateHolder readCert(String certType) throws Exception {
        String certPath = certPath(certType);
        PEMParser parser = new PEMParser(new FileReader(certPath));

        return (X509CertificateHolder) parser.readObject();
    }

    String certPath(String certType) {
        String[] files = new File(CERT_DIR).list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.contains(certType);
            }
        });

        return CERT_DIR + "/" + files[0];
    }

    private CertificateRequestMessage certificateRequestMessage(X500Name issuer, X500Name subject) throws Exception {
        CertificateRequestMessageBuilder builder = new CertificateRequestMessageBuilder(BigInteger.valueOf(CERT_REQ_ID));
        SubjectPublicKeyInfo publicKey= subjectPublicKeyInfo();
        // Date now = new Date();

        return builder
                .setIssuer(issuer)
                .setSubject(subject)
                .setPublicKey(publicKey)
                //.setValidity(now, fiveYearsAfter(now))  // workaround because of the Secunet bug
                .addControl(authenticator())
                .setProofOfPossessionRaVerified()
                .build();
    }

    private Control authenticator() {
        return new AuthenticatorControl("Authenticator");
    }

    private Date fiveYearsAfter(Date now) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.YEAR, 5);

        return cal.getTime();
    }

    private X500Name x500Name(String dirName) {
        return new X500Name(dirName);
    }

    private CMPCertificate signingCertificate() throws Exception {
        String certificateAlias = prop.getProperty("signingCert.alias");

        Certificate certificate = keyStore().getCertificate(certificateAlias);

        return CMPCertificate.getInstance(certificate.getEncoded());
    }

    private PrivateKey signingKey() throws Exception {
        String keyAlias = prop.getProperty("signingKey.alias");
        char[] keyPassword = prop.getProperty("signingKey.password").toCharArray();

        return (PrivateKey) keyStore().getKey(keyAlias, keyPassword);
    }

    private KeyStore keyStore() throws Exception {
        String keyStoreFile = prop.getProperty("keyStore.file");
        char[] keyStorePassword = prop.getProperty("keyStore.password").toCharArray();

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(new FileInputStream(keyStoreFile), keyStorePassword);

        return keyStore;
    }

    private SubjectPublicKeyInfo subjectPublicKeyInfo() throws Exception {
        KeyPair keyPair = keyPair();
        byte[] bytes = keyPair.getPublic().getEncoded();
        ByteArrayInputStream bIn = new ByteArrayInputStream(bytes);
        ASN1InputStream dIn = new ASN1InputStream(bIn);

        return SubjectPublicKeyInfo.getInstance(dIn.readObject());
    }

    private KeyPair keyPair() throws Exception {
        KeyPairGenerator kGen = KeyPairGenerator.getInstance("RSA");
        kGen.initialize(512);

        return kGen.generateKeyPair();
    }

    private PKIMessages pkiMessages(PKIMessage message) {
        return new PKIMessages(message);
    }

    private DERBitString protection(PKIHeader header, PKIBody body) throws Exception {
        ProtectedPart protectedPart = new ProtectedPart(header, body);
        Signature signature = Signature.getInstance(PKCSObjectIdentifiers.sha256WithRSAEncryption.getId());
        signature.initSign(signingKey());
        signature.update(protectedPart.getEncoded());

        return new DERBitString(signature.sign());
    }

    private PKIMessage pkiMessage(PKIHeader header, PKIBody body, DERBitString protection, CMPCertificate certificate) {
        return new PKIMessage(header, body, protection, new CMPCertificate[]{certificate});
    }

    PKIHeader pkiHeader(String transactionID) throws Exception {
        GeneralName sender = generalName(prop.getProperty("SENDER"));
        GeneralName receiver = generalName(prop.getProperty("RECEIVER"));
        byte[] senderKid = stringToByteArray(prop.getProperty("sender.kid"));

        return new PKIHeaderBuilder(PVNO, sender, receiver)
                .setMessageTime(new ASN1GeneralizedTime(new Date()))
                .setProtectionAlg(new AlgorithmIdentifier(PKCSObjectIdentifiers.sha256WithRSAEncryption))
                .setSenderKID(senderKid)
                .setTransactionID(stringToByteArray(transactionID))
                .build();
    }

    private PKIBody pkiBody(MessageType messageType, String subject) throws Exception {
        int bodyType = -1;
        ASN1Encodable messageContent = null;

        switch (messageType) {
            case INIT:
                bodyType = PKIBody.TYPE_CERT_REQ;
                List<CertReqMsg> certMessages = certMessages(subject);
                CertReqMsg[] messages = new CertReqMsg[certMessages.size()];
                messageContent = new CertReqMessages(certMessages.toArray(messages));
                break;
            case POLL:
                bodyType = PKIBody.TYPE_POLL_REQ;
                ASN1Integer certReqId = new ASN1Integer(CERT_REQ_ID);
                messageContent = new PollReqContent(certReqId);
                break;
            case REVOKE:
                bodyType = PKIBody.TYPE_REVOCATION_REQ;
                List<RevDetails> revDetails = revDetails();
                RevDetails[] details = new RevDetails[revDetails.size()];
                messageContent = new RevReqContent(revDetails.toArray(details));
                break;
        }

        return new PKIBody(bodyType, messageContent);
    }

    private GeneralName generalName(String dirName) {
        X500Name x500Name = new X500Name(dirName);

        return new GeneralName(x500Name);
    }

    public byte[] stringToByteArray(String stringID) {
        if (stringID == null) {
            throw new IllegalArgumentException("transactionID id null");
        }

        byte[] transactionID = new byte[stringID.length()];

        if (!stringID.isEmpty()) {
            transactionID = stringID.getBytes(StandardCharsets.US_ASCII);
        }

        return transactionID;
    }

}
