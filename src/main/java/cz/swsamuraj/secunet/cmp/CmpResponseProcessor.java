package cz.swsamuraj.secunet.cmp;

import cz.swsamuraj.secunet.strategy.MessageType;
import org.bouncycastle.asn1.cmp.*;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CmpResponseProcessor {
    
    public void pkiMessages(MessageType messageType, byte[] responseBody) throws Exception {
        PKIMessages pkiMessages = PKIMessages.getInstance(responseBody);
        PKIMessage[] messages = pkiMessages.toPKIMessageArray();

        switch (messageType) {
            case INIT:
                processCertRepMessage(messages);
                break;
            case POLL:
                processPollMessage(messages);
                break;
            case REVOKE:
                processRevMessage(messages);
                break;
        }
    }

    private void processCertRepMessage(PKIMessage[] messages) {
        CertRepMessage message = CertRepMessage.getInstance(messages[0].getBody().getContent());
        CertResponse[] responses = message.getResponse();

        for (CertResponse response : responses) {
            processCertResponse(response);
        }
    }

    private void processPollMessage(PKIMessage[] messages) throws Exception {
        CertRepMessage message = CertRepMessage.getInstance(messages[0].getBody().getContent());
        CertResponse[] responses = message.getResponse();

        for (CertResponse response : responses) {
            processCertificate(response);
        }
    }

    private void processRevMessage(PKIMessage[] messages) {
        RevRepContent revocationResponse = RevRepContent.getInstance(messages[0].getBody().getContent());
        PKIStatusInfo[] statusInfo = revocationResponse.getStatus();

        for (PKIStatusInfo status : statusInfo) {
             processRevResponse(status);
        }
    }

    private void processCertResponse(CertResponse response) {
        String certReqId = response.getCertReqId().toString();
        String status = response.getStatus().getStatus().toString();

        System.out.format("CertReqId[%s] has status: %s%n", certReqId, status);
    }

    private void processCertificate(CertResponse response) throws Exception {
        Certificate certificate = response
                .getCertifiedKeyPair()
                .getCertOrEncCert()
                .getCertificate()
                .getX509v3PKCert();
        String certificateFile = certificateFile(certificate);
        X509Certificate x509Certificate = x509Certificate(certificate);

        writeCertificate(certificateFile, x509Certificate);
    }

    private void processRevResponse(PKIStatusInfo status) {
        BigInteger statusValue = status.getStatus();

        System.out.format("RevRepContent has status: %d%n", statusValue);
    }

    private void writeCertificate(String certificateFile, X509Certificate x509Certificate) throws Exception {
        JcaPEMWriter pemWriter = new JcaPEMWriter(new FileWriter(certificateFile));
        pemWriter.writeObject(x509Certificate);
        pemWriter.flush();
        pemWriter.close();
    }

    String certificateFile(Certificate certificate) {
        String name = certificate.getSubject().toString();
        String type = certType(certificate.getIssuer().toString());

        return "./cert/" + name + "-" + type + ".pem";
    }

    private X509Certificate x509Certificate(Certificate certificate) throws Exception {
        X509CertificateHolder certificateHolder = new X509CertificateHolder(certificate);

        return new JcaX509CertificateConverter().getCertificate(certificateHolder);
    }

    String certType(String issuer) {
        Pattern pattern = Pattern.compile(".+(Sig|Enc|Auth).+");
        Matcher matcher = pattern.matcher(issuer);

        if (matcher.matches()) {
            return matcher.group(1);
        } else {
            return "UNKNOWN";
        }
    }
}
