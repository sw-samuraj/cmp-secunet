# cmp-secunet #

A prototype project for a *secunet CA* client via *CMP*. **Just for study purposes!**

The application doesn't work without further configuration. Unfortunately, this configuration
is proprietary and cannot be published.

## How to run the application ##

 1. Clone the repository.
 1. Set up configuration in the `security` directory (**this is proprietary!**).
 1. Run the application via Gradle *Application Plugin* (see further).
 1. **Browse the source code.**

## Functionality ##

Following is a description of the "happy-day" scenario.

### Initial certificate request ###

Run the command: `gradle run -Pargs="init some-valid-cert-subject"`. The example of the valid
certificate subject could be *C=CZ,CN=SoftWare-Samuraj*.

The application should write *CertReqId[xx] has status: 3* on the output.

### Certificate polling ###

Run the command: `gradle run -Pargs=poll`.

The application will write three certificates to the *cert* directory.

### Certificate revocation ###

Run the command: `gradle run -Pargs=revoke`.

The application will revoke three certificates stored in the *cert* directory. The application
should write *RevRepContent has status: 0* on the output, in case of the successful revocation.

## License ##

The **cmp-secunet** source code is published under [BSD 3-Clause](http://opensource.org/licenses/BSD-3-Clause) license.
